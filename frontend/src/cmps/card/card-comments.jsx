import React, { Component } from 'react';
import { ReactComponent as DescriptionIcon } from '../../assets/svg/card/description.svg';
import { ReactComponent as CloseIcon } from '../../assets/svg/close.svg';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import Divider from '@mui/material/Divider';
import ListItemText from '@mui/material/ListItemText';
import ListItemAvatar from '@mui/material/ListItemAvatar';
import Avatar from '@mui/material/Avatar';
import Typography from '@mui/material/Typography';
import { utilService } from '../../services/util.service';
import { userService } from '../../services/user.service';
import { formatDistance } from 'date-fns';
import { AppBtn } from '../general/app-btn';
// import AddCommentIcon from '@mui/icons-material/AddComment';
import parse from 'html-react-parser';
import Add from "@mui/icons-material/Add";
import IconButton from '@mui/material/IconButton';
// import DeleteIcon from '@mui/icons-material/Delete';
import ClearOutlinedIcon from '@mui/icons-material/ClearOutlined';
import ForumOutlinedIcon from '@mui/icons-material/ForumOutlined';
import ModeOutlinedIcon from '@mui/icons-material/ModeOutlined';
import AddCommentOutlinedIcon from '@mui/icons-material/AddCommentOutlined';
import MarkChatReadOutlinedIcon from '@mui/icons-material/MarkChatReadOutlined';
// MarkChatReadOutlined
import { MentionsInput, Mention } from 'react-mentions'
import { AppAvatar } from '../general/app-avatar';
export class CardComments extends Component {
    state = { isEditing: false, description: '', comment: '', mentionedUsersIds: [] };
    // inputRef = React.createRef();
    // constructor(props) {
    //     super(props);
    //     // create a ref to store the textInput DOM element
    //     this.textInput = React.createRef();
    //     this.focusTextInput = this.focusTextInput.bind(this);
    //   }
    descriptionRef = React.createRef();

    // constructor(props) {
    //     super(props);
    //     // create a ref to store the textInput DOM element
    //     this.inputRef = React.createRef();
    //     this.test = React.createRef();

    //     this.onEdit = this.onEdit.bind(this);
    //   }

    // *this is swhyo emportan
    // handleClick = () => alert(this.test.current.value)


    componentDidMount() {
        // this.loadDescription();
        this.setState({ isEditing: false, disabled: true, mentionedUsersIds: [], commentToEdit: null });
        // this.inputRef = React.createRef()
        // const scrollTo = (ref) => {
        //     if (ref && ref.current /* + other conditions */) {
        //       ref.current.scrollIntoView({ behavior: 'smooth', block: 'start' })
        //     }
        //   }
    }




    onEdit = (idx) => {
        // comment.idx=idx;
        // this.setState({commentToEdit: comment});
        this.setState({ isEditing: true });
    };



    focusTextInput() {
        // Explicitly focus the text input using the raw DOM API
        // Note: we're accessing "current" to get the DOM node
        this.textInput.current.focus();
    }


    componentWillUnmount() {
        clearTimeout(this.timeout);
    }

    loadDescription = () => {
        const { description = '' } = this.props;
    };




    onRemoveComment = (idx) => {
        // const { description = '' } = this.props;

        const { comments = [] } = this.props.card;




        //  this.props.closeCardPopover();
        this.props.updateField({ comments: comments.filter((comment, index) => { return index !== idx }) }, 'DELETE-COMMENT',
            "deleting a button",
        );
        //this.props.closeCardPopover()
        //     closeCardPopover={closeCardPopover}
        console.log("bla bla bla bla bla bla")
    };


    onSave = async () => {
        const { comment } = this.state;
        const mentionedUsersIds = this.extractMentionedUsersIds(comment);
        let revertionedComment = comment;


        // const notification = {
        //     id: utilService.makeId(),
        //     type: 'invite',
        //     title: 'Board Invitation',
        //     user: { _id: this.props.user._id, username: this.props.user.username, fullname:this.props.user.fullname, imgUrl: this.props.user.imgUrl },
        //     isRead: false,
        //     txt: `${ this.props.user.fullname } mentioned you in card ${ this.props.card.title }`,
        //     url:this.props.pathname,
        //     sentAt: Date.now(),
        //   };
        //   let userToUpdate = await userService.getById("621602027dfbf71ac00e45bd");
        //   userToUpdate = { ...userToUpdate, notifications: [notification, ...userToUpdate.notifications] };
        //   userService.update(userToUpdate, false);



        if (!this.state.isEditing) {
            const commentToAdd = {
                id: utilService.makeId(),
                createdAt: Date.now(),
                createdBy: this.props.user._id,
                mentionedUsersIds: mentionedUsersIds,
                commentText: revertionedComment,
            };
            // console.log(revertionedComment);
            const { comments = [] } = this.props.card;
            //  this.props.closeCardPopover();
            this.props.updateField({ comments: [...comments, commentToAdd] }, 'ADD-COMMENT',
                commentToAdd,
            );

            this.setState({ comment: '' })
            this.setState({ disabled: true })

        } else {
            console.log(this.state.commentToEdit)
            let { comments = [] } = this.props.card;

            if (this.state.commentToEdit.idx !== -1) {
                comments[this.state.commentToEdit.idx] = this.state.commentToEdit;
            }


            this.props.updateField({ comments: comments }, 'UPDATE-COMMENT',
                this.state.commentToEdit,
            );

            this.setState({ isEditing: false });


        }

        //   console.log('mentionedUsersIds',mentionedUsersIds);
        // console.log(this.extract(comment,'@@@@','^^^^'));

        // console.log(comment.split('@@@__').pop().split('^^'))
        // this.setState({ isEditing: false });
        // this.props.updateField({ description });
    };

    // onEdit = (comment, idx) => {
    //     comment.idx = idx
    //     this.setState({ isEditing: true, commentToEdit: comment });

    //     // this.inputRef.current.select()
    // };

    onBlur = () => {
        this.timeout = setTimeout(() => {
            if (this.state.isEditing) this.onSave();
        }, 100);
    };


    extractMentionedUsersIds = (str) => {
        const result = str.matchAll(/@@@@(.*?)§§§§/g);

        return Array.from(result, x => x[1]);
    }


    render() {


        const { mentionedUsersIds } = this.state;


        let members = this.props.users;
        // console.log(members);

        members =members ? members.map((user) => {

            // if (this.state.mentionedUsersIds.find(mentionedUserId => mentionedUserId === user._id)) {
            return {
                id: user._id,
                display: user.fullname
            }
            // }
            // return
        }) : [];


        members = members.filter(member => member.id !== this.props.user._id);
        // console.log(members);



        if (mentionedUsersIds.length) {

            members = members.filter(function (member) {
                return !mentionedUsersIds.includes(member.id);
            });


        }
        // console.log(members,)

        // if (members.length) {
        //     members = members.filter((member) => {

        //         //    console.log(this.props.card.members);
        //         return !this.props.card.members.find((cardMember) => cardMember === member.id);
        //     });
        // }


        // console.log(this.props.card.members);


        // myArray = myArray.filter( ( el ) => !toRemove.includes( el ) );

        // console.log("mentionedUserId", mentionedUsersIds);
        return (
            <section className="card-section">
                <div className="section-header">
                    <ForumOutlinedIcon />
                    <div className="flex align-center">
                        <h3 className="section-title">Comments</h3>
                    </div>
                </div>
                <div className="section-data">

                    <List sx={{ width: '100%', bgcolor: 'background.paper', borderRadius: 3 }}>

                        {


                            this.props.card.comments && this.props.card.comments.map((comment, idx) => {
                                let revertionedComment = comment.commentText
                                // .replace(/§§§§__.*@@@/, 'HS');
                                // console.log(revertionedComment.match(/@@@@(.*?)§§§§/))
                                revertionedComment = revertionedComment.replace(new RegExp('@@@@', 'g'), `<a  style='background-color: #eaecf0;   border-radius: 2px; padding: 2px;' href='/user/`);
                                revertionedComment = revertionedComment.replace(new RegExp('§§§§__', 'g'), `'>`);
                                revertionedComment = revertionedComment.replace(new RegExp('@@@', 'g'), `</a>`);

                                let owner = comment.createdBy && this.props.users.find((user) => user._id === comment.createdBy)
                                return (
                                    <>
                                        <ListItem
                                            className="comment"
                                            key={`comment-${comment.id}`}
                                            sx={{ paddingBottom: 0 }}
                                            alignItems="flex-start">
                                            <ListItemAvatar>
                                                {/* <Avatar alt={owner.fullname}
                                                    src={owner.imgUrl}
                                                /> */}
                                                {/* <AppAvatar */}
                                                <AppAvatar member={owner} />
                                            </ListItemAvatar>
                                            <ListItemText
                                                style={{ width: '80%' }}
                                                primary={owner.fullname}
                                                secondary={
                                                    <React.Fragment>
                                                        <Typography
                                                            sx={{
                                                                display: 'inline', inlineSize: 'min-content',
                                                                width: '100%', paddingBottom: 50
                                                            }}
                                                            component="span"
                                                            variant="body2"
                                                            color="text.primary"
                                                        // dangerouslySetInnerHTML={{ __html: revertionedComment.replace(/\n\r?/g, '<br />') }}

                                                        >
                                                            {parse(revertionedComment)}
                                                        </Typography>
                                                        <br></br>
                                                        {/* <br></br> */}

                                                        {/* {comment.commentText} */}
                                                        <span className="created-at" >
                                                            {formatDistance(comment.createdAt, Date.now(), { addSuffix: true })}
                                                        </span>
                                                        {/* </Typography> */}

                                                    </React.Fragment>
                                                }
                                            />
                                            {owner._id === this.props.user._id && <IconButton className="comment-delete-button" aria-label="delete"
                                                name="remove-item"
                                                onClick={ev =>

                                                    // console.log(ev.currentTarget.name)
                                                    this.props.onOpenPopover(ev, {
                                                        item: { title: "blabla" },
                                                        onRemoveItem: () => this.onRemoveComment(idx),
                                                        msg: "are you sure you want to delete this comment ?",
                                                        itemType: 'comment',
                                                    })
                                                }

                                                style={{ position: "absolute", top: 1, right: 1 }} color="primary">

                                                <ClearOutlinedIcon />
                                            </IconButton>
                                            }






                                            {owner._id === this.props.user._id && <IconButton className="comment-delete-button" aria-label="delete"
                                                name="remove-item"
                                                // onClick={() => this.onEdit(comment, idx)}
                                                // onClick={this.focusTextInput}
                                                onClick={() => {

                                                    comment.idx = idx;
                                                    this.setState({ commentToEdit: comment })


                                                    this.onEdit(idx)
                                                }}

                                                style={{ position: "absolute", top: 1, right: 28 }} color="primary">

                                                <ModeOutlinedIcon />
                                            </IconButton>
                                            }





                                        </ListItem>

                                        <Divider
                                            sx={{ marginLeft: '15%', paddingTop: 0 }}

                                            variant="inset" component="li" />

                                    </>
                                )





                            })
                        }







                        <ListItem alignItems="flex-start">
                            <ListItemAvatar>
                                {/* <Avatar alt="Cindy Baker" src={this.props.user.imgUrl} /> */}
                                <AppAvatar member={this.props.user} />

                            </ListItemAvatar>
                            <ListItemText
                                primary={this.props.user.fullname}
                                secondary={
                                    <React.Fragment>


                                        <>
                                            <MentionsInput value={this.state.isEditing ? this.state.commentToEdit.commentText : this.state.comment}

                                                className="comments-textarea"
                                                style={{ width: '100%', minHeight: 40 }}
                                                // ref={this.textInput}
                                                ref={this.descriptionRef}

                                                // ref={(ref)=>this.inputRef=ref}
                                                // ref={this.test}
                                                onChange={event => {
                                                    console.log(event.target.value);


                                                    if (event.target.value) {

                                                        if (!this.state.isEditing) {

                                                            if (event.target.value.trim().length) {

                                                                let mentionedUsersIds = this.extractMentionedUsersIds(event.target.value)
                                                                this.setState({ disabled: false, mentionedUsersIds: mentionedUsersIds })

                                                            }
                                                            this.setState({ comment: event.target.value })
                                                        } else {

                                                            if (event.target.value.trim().length) {

                                                                let mentionedUsersIds = this.extractMentionedUsersIds(event.target.value)
                                                                this.setState({ disabled: false, commentToEdit: { ...this.state.commentToEdit, mentionedUsersIds: mentionedUsersIds }, mentionedUsersIds: mentionedUsersIds })
                                                                // console.log(mentionedUsersIds,this.state.commentToEdit.commentText);

                                                            }
                                                            this.setState({ commentToEdit: { ...this.state.commentToEdit, commentText: event.target.value } })

                                                        }


                                                    } else {
                                                        this.setState({ disabled: true, mentionedUsersIds: [], comment: '' })


                                                        if (this.state.isEditing) {
                                                            this.setState({ disabled: true, mentionedUsersIds: [], commentToEdit: { ...this.state.commentToEdit, commentText: '' } })

                                                        }



                                                    }




                                                }


                                                }>
                                                <Mention
                                                    trigger='@'
                                                    data={members}
                                                    // style={{
                                                    //     backgroundColor: "red",
                                                    //     position:'relative'

                                                    //   }}

                                                    markup='@@@@__id__§§§§____display__@@@'


                                                />
                                            </MentionsInput>

                                            <div className="add-controls">

                                                <AppBtn
                                                    // style={!this.state.comment &&{}}
                                                    // disabled={!this.state.comment:true?false}
                                                    disabled={this.state.disabled}
                                                    name="add-comment"
                                                    onClick={this.onSave
                                                    }>
                                                    {this.state.isEditing ? <MarkChatReadOutlinedIcon /> : <AddCommentOutlinedIcon />}
                                                    <div style={{ paddingLeft: 5 }}> {this.state.isEditing ? "update comment" : "Add comment"}</div>
                                                </AppBtn>
                                                {/* 
                                                <button onClick={this.onSave} className="btn-add">
                                                    Save
                                                </button> */}
                                                {(this.state.comment || this.state.isEditing) && <CloseIcon className="close-icon" onClick={() => {



                                                    if (this.state.isEditing) {

                                                        this.setState({ comment: '' })
                                                        this.setState({ isEditing: false })

                                                    }





                                                }} />
                                                }  </div>
                                        </>



                                    </React.Fragment>
                                }
                            />
                        </ListItem>

                    </List>

                </div>

            </section >
        );
    }
}
