import React, { Component } from 'react';
import { cardService } from '../../services/board-services/card.service';
import { CardChecklist } from './checklist/card-checklist';

export class CardChecklists extends Component {
  state = { addingChecklistId: null };
  addBtnRef = React.createRef();
  card = this.props.card;

  onAddingItem = (addingChecklistId, inputRef) => {
    this.setState({ addingChecklistId }, () => {
      inputRef && inputRef.current.focus();
    });
  };

  onAddItem = (checklist, item) => {
    const updatedCard = cardService.addChecklistItem(this.card, checklist.id, item);
    const { checklists } = updatedCard;


    // console.log({ checklists },'UPDATE-CHECK-LIST',item);
    // checklists = updatedChecklists
    this.props.updateField({ checklists }, 'ADD-ITEM-TO-CHECK-LIST', checklist);

    // this.props.updateField({ checklists });
  };

  onRemoveItem = (checklist, itemId) => {
    const updatedCard = cardService.removeChecklistItem(this.card, checklist.id, itemId);
    const { card, affectedChecklist, removedItem } = updatedCard;
    // this.props.updateField({ checklists });
    let checklists = card.checklists

    this.props.updateField({ checklists },'REMOVE-ITEM-FROM-CHECK-LIST',{checkListTitle:affectedChecklist.title,removedItemtitle:removedItem.title});



  };

  onDeleteChecklist = checklistId => {

    const updatedCard = cardService.deleteChecklist(this.card, checklistId);
    let { card, deletedCheckList } = updatedCard;
    let checklists = card.checklists

    // console.log(updatedCard)
    // delete deletedCheckList.items
    // this.props.updateField({ card.checklists });
    this.props.updateField({ checklists },'DELETE-CHECK-LIST',{id:deletedCheckList.id,title:deletedCheckList.title});


  };


  onUpdateChecklist = updatedChecklist => {
    let { checklists } = this.card;
    const updatedChecklists = checklists.map(checklist =>
      checklist.id === updatedChecklist.id ? updatedChecklist : checklist
    );
    checklists = updatedChecklists
    this.props.updateField({ checklists },'UPDATE-CHECK-LIST',{id:updatedChecklist.id,title:updatedChecklist.title});
   
  };

  render() {
    this.card = this.props.card;
    const {
      checklists,
      updateField,
    } = this.props;
    const { addingChecklistId } = this.state;
    if (!checklists) return <></>;
    return (
      <section className="card-section card-checklists">
        {checklists.map(checklist => (
          <CardChecklist
            key={checklist.id}
            card={this.card}
            checklist={checklist}
            isAdding={addingChecklistId === checklist.id}
            onAddingItem={this.onAddingItem}
            onAddItem={this.onAddItem}
            onRemoveItem={this.onRemoveItem}
            onUpdateChecklist={this.onUpdateChecklist}
            onDeleteChecklist={this.onDeleteChecklist}
            updateField={updateField}
          />
        ))}
      </section>
    );
  }
}
