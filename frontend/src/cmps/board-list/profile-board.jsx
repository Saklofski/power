import React, { useState, useEffect } from 'react';
// import { BoardList } from './board-list';
// import { LoaderPage } from '../loader/loader-page';
import { Avatar } from '@material-ui/core';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import { styled } from '@mui/material/styles';
import { AppAvatar } from '../general/app-avatar';
// import { useNavigate } from "react-router-dom";
import IconButton from '@mui/material/IconButton';
import RemoveCircleIcon from '@mui/icons-material/RemoveCircle';
import BackupIcon from '@mui/icons-material/Backup';

import ClearOutlinedIcon from '@mui/icons-material/ClearOutlined';
// import IconButton from '@mui/material/IconButton';
// import DeleteIcon from '@mui/icons-material/Delete';
// import Box from '@mui/material/Box';
// import Tabs from '@mui/material/Tabs';
// import Tab from '@mui/material/Tab';
// import Button from '@mui/material/Button';
import { connect } from 'react-redux';
// import { createBoard } from '../../store/actions/board.actions';
import { onUpdateUser } from '../../store/actions/user.actions';
import { cloudinaryService } from '../../services/cloudinary-service';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
// import 'react-tabs/style/react-tabs.css';
// ../../../services/cloudinary-service
const Input = styled('input')({
  display: 'none',
});








export function _ProfileBoard(props) {

  const [value, setValue] = React.useState(0);


  const [fullname, setFullname] = React.useState(props.user.fullname);



  const handleFullNameChange = (e) => {
    console.log(e.target.value.trim().length)
    setFullname(e.target.value);

    // console.log(event);
  };
  const handleBioChange = (e) => {
    console.log(e.target.value.trim().length)


    setBio(e.target.value);

    // console.log(event);
  };
  // setBio

  const [textarea, setTextarea] = useState(
    "The content of a textarea goes in the value attribute"
  );
  const [bio, setBio] = useState(props.user.bio || " ");
  const onUploadProfileImg = async (ev) => {
    try {
      // this.setState({ isLoading: true })
      const img = await cloudinaryService.uploadImg(ev);
      // this.setState({ isLoading: false })
      // this.onSetBgImg(img)
      var updatedUser = props.user;
      updatedUser.imgUrl = img.url;
      props.onUpdateUser(updatedUser)


    } catch (err) {
      this.setState({ errMsg: 'That file size exceeds the 10MB limit' });
      console.error('upload file error', err);
    }
  }



  const onDeleteProfileImg = async () => {
    try {
      // this.setState({ isLoading: true })
      // const img = await cloudinaryService.uploadImg();
      // this.setState({ isLoading: false })
      // this.onSetBgImg(img)
      var updatedUser = props.user;
      updatedUser.imgUrl = "";
      props.onUpdateUser(updatedUser)


    } catch (err) {
      this.setState({ errMsg: 'That file size exceeds the 10MB limit' });
      console.error('upload file error', err);
    }
  }






  const onUploadUserDetails = async (ev) => {
    try {
      // this.setState({ isLoading: true })
      // const img = await cloudinaryService.uploadImg(ev);
      // this.setState({ isLoading: false })
      // this.onSetBgImg(img)
      if (fullname) {
        var updatedUser = props.user;

        updatedUser.bio = bio;
        updatedUser.fullname = fullname;

        props.onUpdateUser(updatedUser)
        props.history.replace('/board')
      }  // updatedUser.imgUrl = img.url;

      //  console.log( bio.trim().length)




      // props.onUpdateUser(updatedUser)


    } catch (err) {
      this.setState({ errMsg: 'That file size exceeds the 10MB limit' });
      console.error('upload file error', err);
    }
  }







  return (
    <section style={{
      alignItems: 'center',
      justifyContent: 'center',
      width: '100%',
      backgroundColor: '#f4f5f7'
      // padding: 32,
    }}>
      <div style={{
        // display: 'flex',
        // backgroundColor: '#f4f5f7',

        alignItems: 'center',
        justifyContent: 'center',
        width: '100%',
        padding: '32px 32px 16px 32px',
      }} >
        <div style={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          width: '100%',
          // padding: 32
        }}>
          {/* {<h3 className="star">Starred boards!</h3>} */}




          {/* <section > */}
          <div style={{
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            padding: '20px 0',
          }}>
            {/* <Link to="/profile"> */}
            {/* <a>Image</a> */}


            <div className="avatarContainer" style={{ position: 'relative' }}>


              {(props.user.imgUrl ) &&
                <IconButton
                  onClick={onDeleteProfileImg}
                  // className="avatar-delete-button"
                  style={{ position: "absolute", top: 0, right: -7, padding: 0, margin: 0, zIndex: 999999 }} color="primary">
                  <RemoveCircleIcon style={{ color: '#c1c7d0' }} />
                </IconButton>
              }


              <input type="file" id="file" onChange={onUploadProfileImg} style={{ display: "none" }} />
              <label for="file">
               <BackupIcon className="middle"  />
              </label>

                <AppAvatar isProfile={true} isProfilePath={true} style={{ height: "48px", width: '48px' }} member={props.user} />
              {/* </Link> */}

            </div>

            <div style={{
              display: 'flex',
              alignItems: 'baseline',
              marginLeft: '16px'
            }}>
              <div className="fullname" style={{
                fontSize: '24px',
                fontWeight: '500',
                lineHeight: '28px',
                color: '#0C3953',
                marginRight: '10px'
              }}>{props.user.fullname}</div>
              <div style={{
                fontSize: '12px',
                lineHeight: '20px',
                color: '#5e6c84',
              }} className="username">{"@" + props.user.username.split("@")[0]}</div>
            </div>
          </div>
          {/* </section> */}


        </div >



      </div >

      <section style={{
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        // width: 'auto',
        // padding: 32
        backgroundColor: 'white'
      }}>
        <Tabs>
          <TabList className="react-tabs">
            <Tab   >Profile and visability</Tab>
            {/* <Tab >Activity</Tab>
            <Tab >Cards</Tab>
            <Tab >Settings</Tab> */}
            {/* <Tab >Title 4</Tab> */}
            {/* <Tab >Title 5</Tab> */}
          </TabList>

          <TabPanel>
            <div>
              <img src="https://a.trellocdn.com/prgb/dist/images/member-home/taco-privacy.eff3d701a9c3a71105ea.svg" alt="" className='responsive' ></img>
              <h1>Manage your personal information</h1>
              <div className='desc-prev'> This is an Powerful account. Edit your personal information and visibility settings through your Powerful profile.

                To learn more, view our Terms of Service or Privacy Policy.</div>

              <h3>About</h3>

              {/* <form> */}
              <span>User name</span>

              <input
                type="text"
                name="username"
                value={fullname || ""}
                onChange={handleFullNameChange}
              />
              {/* <span>Bio</span>
              <textarea value={bio} onChange={handleBioChange} /> */}


              {/* <input type="submit" value="Submit" /> */}
              <button className="sub" onClick={onUploadUserDetails} > Save </button>

              {/* </form> */}
            </div>
          </TabPanel>
          <TabPanel>
            <h2>Any content 2</h2>
            {/* <img src="https://a.trellocdn.com/prgb/dist/images/member-home/taco-privacy.eff3d701a9c3a71105ea.svg" alt="" className='=responsive' ></img> */}

          </TabPanel>
          <TabPanel>
            <h2>Any content 3</h2>
            {/* <img src="https://a.trellocdn.com/prgb/dist/images/member-home/taco-privacy.eff3d701a9c3a71105ea.svg" alt="" className='=responsive' ></img> */}
            {/* <img src="https://a.trellocdn.com/prgb/dist/images/member-home/taco-privacy.eff3d701a9c3a71105ea.svg"></img> */}

          </TabPanel>
          <TabPanel>
            <h2>Any content 3</h2>
            {/* <img src="https://a.trellocdn.com/prgb/dist/images/member-home/taco-privacy.eff3d701a9c3a71105ea.svg" alt="" className='=responsive' ></img> */}
            {/* <img src="https://a.trellocdn.com/prgb/dist/images/member-home/taco-privacy.eff3d701a9c3a71105ea.svg"></img> */}

          </TabPanel>
        </Tabs>
      </section>

    </section>



  );
}
const mapDispatchToProps = {
  // createBoard,
  onUpdateUser,
};



const style = {


  tab: {
    backgroundColor: '#dfe1e6',
    border: ' 1px solid #dfe1e6',
    borderBottom: 0,
    borderRadius: ' 3px 3px 0 0',
    boxSizing: 'border-box',
    display: 'inline-block',
    fontWeight: 700,
    margin: '2px 2px 0',
    padding: '8px 20px',
    textDecoration: 'none',

  }
}






const mapStateToProps = state => {
  return {
    boards: state.boardModule.boards,
    user: state.userModule.user,
  };
};

export const ProfileBoard = connect(mapStateToProps, mapDispatchToProps)(_ProfileBoard);
