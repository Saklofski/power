import { httpService } from './http.service';
import { socketService } from './socket.service';
import _ from 'lodash';
import { cardService } from '../services/board-services/card.service';
import { utilService } from './util.service';
import { AppAvatar } from '../cmps/general/app-avatar';

const STORAGE_KEY_LOGGEDIN_USER = 'loggedinUser';

const SOCKET_EVENT_SET_USER = 'set-user-socket';
const SOCKET_EVENT_UNSET_USER = 'unset-user-socket';

export const userService = {
  login,
  logout,
  signup,
  getLoggedinUser,
  getUsers,
  getById,
  update,
  sendMails
};

window.userService = userService;

function getUsers(filterBy) {
  return httpService.get(`user?name=${filterBy.name}`);
}

async function getById(userId) {
  const user = await httpService.get(`user/${userId}`);
  return user;
}

async function update(user, isCurrUser = true) {
  user = await httpService.put(`user/${user._id}`, user);
  return isCurrUser ? _saveLocalUser(user) : user;
}

async function login(userCred) {


  // console.log("$$$$$$$$$$$$$$$$$$$",userCred);
  // with service worker - use in production:
  const user = await httpService.post('auth/login', { ...userCred });
  console.log(user)
  // no service worker - use in developement:
  // const user = await httpService.post('auth/login', userCred);
  socketService.emit(SOCKET_EVENT_SET_USER, user._id);
  if (user) return _saveLocalUser(user);
}


function _DynamicMail(createdBy, card, type, values, board) {

  let cardUrl
  if (type !== 'invite') {
    cardUrl = type ? `http://localhost:3000/board/${board._id}/card/${card.id}` : '/';
  } 

  const BASE_URL = process.env.NODE_ENV === 'production' ? '/api/' : '//localhost:3030/api/';

  // let boardUrl=`http://localhost:3000/board/${board._id}`;
  // const CardLink = () => <Link to={cardUrl} className="link">{card.title}</Link>;

  /* const key = 'AIzaSyDgw0mWmcS4OoFUyLUj5oNbfo4KGzpHiYA'; */
  switch (type) {
    case 'ADD-CARD':
      return `<span>Added <a href="${cardUrl}">${card.title}</a> to ${values.listTitle}</span>`;
    case 'UPDATE-TITLE':
      return `<span>Updated title on card <a href="${cardUrl}">${card.title}</a></span>`;
    case 'ADD-MEMBER':
      if (values.member._id === createdBy._id) return `<span>joined to <a href="${cardUrl}">${card.title}</a></span>`;
      return `<span>Added <span className="created-by">${values.member.fullname || values.member.username}</span> to <a href="${cardUrl}">${card.title}</a></span>`;
    case 'REMOVE-MEMBER':
      if (values.member._id === createdBy._id) return `<span>left <a href="${cardUrl}">${card.title}</a></span>`;
      return `<span>Removed <span className="created-by">${values.member.fullname || values.member.username}</span> from <a href="${cardUrl}">${card.title}</a></span>`;
    case 'ADD-CHECKLIST':
      return `<span>Added checklist "${values.title}" to <a href="${cardUrl}">${card.title}</a></span>`;
    case 'CHECKLIST-COMPLETE':
      return `<span>Completed "${values.title}" on <a href="${cardUrl}">${card.title}</a></span>`;
    case 'CHANGE-DESCRIPTION':
      return `<span>Changed the description of <a href="${cardUrl}">${card.title}</a></span>`;
    case 'ADD-ITEM-TO-CHECK-LIST':
      return `<span>Added an item to check list ${values.title} on   <a href="${cardUrl}">${card.title}</a></span>`;

    case 'DELETE-CHECK-LIST':
      return `<span>Deleted checklist ${values.title} on <a href="${cardUrl}">${card.title}</a> </span>`;

    case 'REMOVE-ITEM-FROM-CHECK-LIST':
      return `<span>Removed item "${values.removedItemtitle}"  of checklist ${values.checkListTitle} on  <a href="${cardUrl}">${card.title}</a></span>`;


    case 'UPDATE-ITEM-FROM-CHECK-LIST':
      return `<span>Changed item ${values.updatedItemtitle}  of checklist ${values.checkListTitle} on  <a href="${cardUrl}">${card.title}</a></span>`;


    case 'UPDATE-CHECK-LIST':
      // return <span>{`changed item ${notification.values.updatedItemtitle}  of checklist ${notification.values.checkListTitle} on `} <CardLink /></span>;
      return `<span>Updated checklist ${values.title} on  <a href="${cardUrl}">${card.title}</a></span>`;


    /* case 'ADD-LOCATION':
      const { lat, lng } = values.location;
      return <div>
        <div>added location {values.title} to <a href="${cardUrl}">${card.title}</a></div>
        <img src={`https://maps.googleapis.com/maps/api/staticmap?key=${ key }&libraries=places&zoom=13&size=225x66&markers=icon%3Ahttps://trello.com/images/location-marker.png%7C${ lat },${ lng }`}
          alt='card-location' />
      </div>; */

    case 'REMOVE-ATTACHMENT':
      return `<span>Removed an attachment from  <a href="${cardUrl}">${card.title}</a></span>`;

    case 'UPDATE-ATTACHMENT':
      return `<span>Updated an attachment on  <a href="${cardUrl}">${card.title}</a></span>`;

    // UPDATE-ATTACHMENT
    case 'ADD-ATTACHMENT':
      // const isValidImg = utilService.isValidImg(values.attachment.url);
      const AttachmentName = _.truncate(values.attachment.name, { length: 20 }) || _.truncate(values.attachment.url, { length: 20 });
      return `<div>
        <div>attached 
           <a className="link" target="_blank" rel="noreferrer" download href=${values.attachment.url}>${AttachmentName} </a>
          to <a href="${cardUrl}">${card.title}</a></div>
          </div>`;
    case 'ADD-DUE-DATE':
      return ` <span>Set the due date on <a href="${cardUrl}">${card.title}</a> to 
        <span className={due-date ${cardService.checkDueDate({ date: values.date, isComplete: false })}}>
          <span className="due-date-icon"></span>
          <span>${utilService.getFormattedDate(values.date, true)}</span>
        </span>
      </span>`;
    case 'MARK-DUE-DATE':
      return `<span>Marked the due date on <a href="${cardUrl}">${card.title}</a> ${values.dueDate.isComplete ? 'Complete' : 'Incomplete'} </span>`;
    case 'ADD-COVER':
      return `<div>Set cover image on <a href="${cardUrl}">${card.title}</a></div>`;
    case 'SET-COLOR-COVER':
      return `<div>Set ${values.color} as a cover color  on <a href="${cardUrl}">${card.title}</a></div>`;


    case 'REMOVE-COVER':
      return `<span>Removed cover image on <a href="${cardUrl}">${card.title}</a></span>`;
    case 'ADD-COMMENT':
      return `<span>Commented on <a href="${cardUrl}">${card.title}</a></span>`;
      case 'DELETE-COMMENT':
        return `<span> Deleted his comment on <a href="${cardUrl}">${card.title}</a></span>`;
      case 'UPDATE-COMMENT':
        return `<span>Updated his comment on <a href="${cardUrl}">${card.title}</a></span>`;
    case 'ADD-LABEL':
      return `<span>Added label ${values.label.title}on <a href="${cardUrl}">${card.title}</a></span>`;
    case 'REMOVE-LABEL':
      return `<span>Removed label ${values.label.title} from <a href="${cardUrl}">${card.title}</a></span>`;

    case 'invite': 
     return `<span>Invited you to  join Board <a href="http://localhost:3000/invite/${board._id}">${board.title}</a> !</span>`;
      
    case 'ARCHIVE-CARD':
      // return <span>{`changed item ${notification.values.updatedItemtitle}  of checklist ${notification.values.checkListTitle} on `} <CardLink /></span>;
      return `<span>Moved <a href="${cardUrl}">${card.title}</a>  to archive </span>`;
    case 'REMOVE-CARD':
      // return <span>{`changed item ${notification.values.updatedItemtitle}  of checklist ${notification.values.checkListTitle} on `} <CardLink /></span>;
      return `<span>Removed ${card.title}</span>`;


    case 'COPY-CARD':
      // return <span>{`changed item ${notification.values.updatedItemtitle}  of checklist ${notification.values.checkListTitle} on `} <CardLink /></span>;
      return `<span>Copied ${card.title}  in list ${values.listTitle}</span>`;
      case 'MOVE-CARD':
        // return <span>{`changed item ${notification.values.updatedItemtitle}  of checklist ${notification.values.checkListTitle} on `} <CardLink /></span>;
        // return <span>{`moved ${notification.card.title}` to}   </span>;
        return `<span>Moved <a href="${cardUrl}">${card.title}</a> to list ${values.listTitle}</span>`;


    default:



      return `<span></span>`;
  }
}


async function sendMails(createdBy, card, type, values, board, userToUpdate = null) {

  if (type === 'invite' && userToUpdate) {
    
    const mailData = {
      from: 'powerfulscrum@gmail.com',
      to: userToUpdate.username,
      subject: 'Board Invitation',
      text: 'That was easy!',
      html: "<h3>" + createdBy.username + "</h3>" + "<h2>" + createdBy.fullname + " " + _DynamicMail(createdBy, card, type, values, board) + "<h2>" +
      "<img style=width:50px;height:50px src=https://pngimage.net/wp-content/uploads/2018/06/trello-logo-png-5.png>Powerful"
    }

    const mailResponse = await httpService.post('user/sendmail/members', { ...mailData });

  } else {
    const mailData = {
      from: 'powerfulscrum@gmail.com',
      to: [],
      subject: type === 'invite' ? 'Board Invitation' : 'Update',
      text: 'That was easy!',
      html:"<h3>" + createdBy.username + "</h3>" + "<h2>" + createdBy.fullname + " " + _DynamicMail(createdBy, card, type, values, board) + "<h2>" +
      "<img style=width:50px;height:50px src=https://pngimage.net/wp-content/uploads/2018/06/trello-logo-png-5.png>Powerful"
    }


    mailData.to = card.members.map(member => { return member.username });

    const mailResponse = await httpService.post('user/sendmail/members', { ...mailData });
  }
}






async function signup(userCred) {
  const user = await httpService.post('auth/signup', userCred);
  socketService.emit(SOCKET_EVENT_SET_USER, user._id);
  return _saveLocalUser(user);
}

async function logout() {
  localStorage.removeItem(STORAGE_KEY_LOGGEDIN_USER);
  socketService.emit(SOCKET_EVENT_UNSET_USER);
  return await httpService.post('auth/logout');
}

function _saveLocalUser(user) {
  localStorage.setItem(STORAGE_KEY_LOGGEDIN_USER, JSON.stringify(user));
  return user;
}

function getLoggedinUser() {
  return JSON.parse(localStorage.getItem(STORAGE_KEY_LOGGEDIN_USER) || 'null');
}

(async () => {
  var user = getLoggedinUser();
  if (user) socketService.emit(SOCKET_EVENT_SET_USER, user._id);
})();


