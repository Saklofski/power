const dbService = require('../../services/db.service')
const logger = require('../../services/logger.service')
const ObjectId = require('mongodb').ObjectId

module.exports = {
    query,
    getById,
    getByUsername,
    update,
    add
}

async function query(filterBy = {}, lookingForIds = false) {
    const collection = await dbService.getCollection('user')
    let users
    try {


        if (lookingForIds) {

            let objectIds = filterBy.map(id => { return ObjectId(id) })
            users = await collection.find({ _id: { $in: objectIds } }).toArray()
            users = users.map(user => {
                delete user.password
                user.createdAt = ObjectId(user._id).getTimestamp()
                return user
            })
            return users


        }

        const criteria = _buildCriteria(filterBy)

        users = await collection.find(criteria).toArray()
        users = users.map(user => {
            delete user.password
            user.createdAt = ObjectId(user._id).getTimestamp()
            return user
        })
        return users
    } catch (err) {
        logger.error('cannot find users', err)
        throw err
    }
}

async function getById(userId) {
    try {
        const collection = await dbService.getCollection('user')
        let user = await collection.aggregate([{
            $match: { _id: ObjectId(userId) },
        }, {
            $unwind: {
                path: '$notifications',
                preserveNullAndEmptyArrays: true
            }
        }, {
            $unwind: {
                path: '$notifications.user',
                preserveNullAndEmptyArrays: true

            }
        }
            ,
        {
            $lookup: {
                from: 'user',
                localField: 'notifications.user._id',
                foreignField: '_id',
                as: 'notifications.user'
            }
        }
            , {
            $unwind: {
                path: '$notifications.user',
                preserveNullAndEmptyArrays: true

            }
        }, {
            $group: {
                _id: '$_id',
                username: {
                    $first: '$username'
                },
                fullname: {
                    $first: '$fullname'
                },
                imgUrl: {
                    $first: '$imgUrl'
                },
                notifications: {
                    $push: '$notifications'
                }
            }
        },
        {
            $project: {
                //   'createdBy.password': 0,
                //   'createdBy.starredBoardsIds': 0,
                //   'createdBy.notifications': 0,


                //   'members.password': 0,
                //   'members.notifications': 0,
                //   'members.starredBoardsIds': 0,


                'notifications.user.password': 0,
                'notifications.user.notifications': 0,
                'notifications.user.mentions': 0,
                'notifications.user.starredBoardsIds': 0,
                'notifications.card.members': 0,
                'notifications.card.comments': 0,
                'notifications.card.cover': 0,
                // 'notifications.card.members': 0,

                // 'members.password': 0,
                // 'members.starredBoardsIds': 0,

            },
        }


        ]).toArray();

        // if (!user[0].starredBoardsIds) {
        //     user[0].starredBoardsIds = []
        // }

        // if (!user[0].notifications[user[0].notifications.length]) {
        //     // console.log("Object.keys", user[0].notifications[user[0].notifications.length]);
        // user[0].notifications.pop();

        // }

        if (user[0].notifications.length != 0) {
            user[0].notifications = user[0].notifications.filter(element => {
                if (Object.keys(element).length !== 0) {
                    return true;
                }

                return false;
            });
        }
        // delete user[0].password
        return user[0]
    } catch (err) {
        logger.error(`while finding user ${userId}`, err)
        throw err
    }
}
async function getByUsername(username) {
    try {
        const collection = await dbService.getCollection('user')
        let user = await collection.aggregate([{
            $match: { username: username },
        }, {
            $unwind: {
                path: '$notifications',
                preserveNullAndEmptyArrays: true
            }
        }, {
            $unwind: {
                path: '$notifications.user',
                preserveNullAndEmptyArrays: true

            }
        }
            ,
        {
            $lookup: {
                from: 'user',
                localField: 'notifications.user._id',
                foreignField: '_id',
                as: 'notifications.user'
            }
        }
            , {
            $unwind: {
                path: '$notifications.user',
                preserveNullAndEmptyArrays: true

            }
        }, {
            $group: {
                _id: '$_id',
                username: {
                    $first: '$username'
                },
                password: {
                    $first: '$password'
                },
                fullname: {
                    $first: '$fullname'
                },
                imgUrl: {
                    $first: '$imgUrl'
                },
                starredBoardsIds: {
                    $first: '$starredBoardsIds'
                },
                notifications: {
                    $push: '$notifications'
                }
            }
        },
        {
            $project: {
                //   'createdBy.password': 0,
                //   'createdBy.starredBoardsIds': 0,
                //   'createdBy.notifications': 0,


                //   'members.password': 0,
                //   'members.notifications': 0,
                //   'members.starredBoardsIds': 0,


                'notifications.user.password': 0,
                'notifications.user.notifications': 0,
                'notifications.user.mentions': 0,
                'notifications.user.starredBoardsIds': 0,
                'notifications.card.members': 0,
                'notifications.card.comments': 0,
                'notifications.card.cover': 0,
                // 'members.password': 0,
                // 'members.starredBoardsIds': 0,
                // 'starredBoardsIds':  {$ifNull: [ "$starredBoardsIds", []] }

            },
        }


        ]).toArray();
        if (user[0]) {// user[0]
            if (!user[0].starredBoardsIds) {
                user[0].starredBoardsIds = []
            }


            if (user[0].notifications.length != 0) {
                user[0].notifications = user[0].notifications.filter(element => {
                    if (Object.keys(element).length !== 0) {
                        return true;
                    }

                    return false;
                });
            }
        }
        // delete user[0].password
        return user[0]




        // return user
    } catch (err) {
        logger.error(`while finding user ${username}`, err)
        throw err
    }
}

async function update(user) {
    try {

        let objectifiedNotifications
        if (user.notifications) {
            objectifiedNotifications = user.notifications.map(notification => {


                if (notification.user) {
                    notification.user._id = ObjectId(notification.user._id)

                }

                return notification



            }


            )
            // console.log("objectifiedNotifications", objectifiedNotifications);
            user.notifications = objectifiedNotifications
        }


        // peek only updatable fields!
        const userToSave = {
            ...user,
            _id: ObjectId(user._id), // needed for the returnd obj
            starredBoardsIds: user.starredBoardsIds || [],
            // notifications:

        }

        console.log("objectifiedNotifications", objectifiedNotifications);

        const collection = await dbService.getCollection('user')
        await collection.updateOne({ _id: userToSave._id }, { $set: userToSave })
        return userToSave;
    } catch (err) {
        logger.error(`cannot update user ${user._id}`, err)
        throw err
    }
}

async function add(user) {
    try {
        // peek only updatable fields!
        const userToAdd = {
            username: user.username,
            password: user.password,
            fullname: user.fullname,
            imgUrl: user.imgUrl || '',
            mentions: [],
            starredBoardsIds: [],
            notifications: []
        }
        const collection = await dbService.getCollection('user')
        await collection.insertOne(userToAdd)
        delete userToAdd.password
        return userToAdd
    } catch (err) {
        logger.error('cannot insert user', err)
        throw err
    }
}

function _buildCriteria(filterBy) {
    const criteria = {}
    if (filterBy.name) {
        const txtCriteria = { $regex: filterBy.name, $options: 'i' }
        criteria.$or = [
            {
                username: txtCriteria
            },
            {
                fullname: txtCriteria
            }
        ]
    }
    return criteria
}




